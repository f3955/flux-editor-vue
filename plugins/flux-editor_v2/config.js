export default {
  components: {
    image: { component: 'fluxPluginImage', options: {} },
    video: { component: 'fluxPluginVideo', options: {} },
    audio: { component: 'fluxPluginAudio', options: {} },
    section: {
      component: 'FluxPluginSection',
      options: {
        sections: ['section-frame1', 'section-frame2', 'section-end'],
      },
    },
    code: { component: 'fluxPluginCode', options: {} },
  },
  references: {
    text: {
      scheme: 'text',
      attributes: {
        source: '',
      },
      id: null,
    },
    image: {
      scheme: 'image',
      attributes: {
        src: null,
        href: null,
        href_target: '_self',
        title: null,
        credentials: null,
        credentials_align: 'text-center',
        aspect: 'fluid',
        fit: 'contain',
      },
      id: null,
    },
    video: {
      scheme: 'video',
      attributes: {
        id: null,
        src: null,
        source: 'YOUTUBE',
        aspect: '16/9',
        title: null,
        credentials: null,
        credentials_align: 'text-center',
      },
      id: null,
    },
    audio: {
      scheme: 'audio',
      attributes: {
        src: null,
        title: null,
        credentials: null,
        credentials_align: 'text-center',
      },
      id: null,
    },
    section: {
      scheme: 'section',
      attributes: {
        type: 'boxed',
      },
      id: null,
    },
    code: {
      scheme: 'code',
      attributes: {
        source: '',
        aspect: '1/1',
      },
      id: null,
    },
  },
}
