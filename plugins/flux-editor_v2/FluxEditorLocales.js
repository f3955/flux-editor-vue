export default function Locales() {
  const locales = {
    FR: {
      _self: 'Même fenêtre',
      _blank: 'Nouvelle fenêtre',
      cover: 'Occuper tout l’espace',
      contain: 'Voir tout',
      Title: 'Légende',
      Credits: 'Crédits',
      Source: 'Source',
      Link: 'Lien',
    },
  }

  function i18n(str, lang) {
    console.log(str, lang)

    if (this.$t) {
      return this.$t(`FLUXEDITOR.${str}`)
    }
    if (locales[lang] && locales[lang][str]) {
      return locales[lang] && locales[lang][str]
    }
    return str
  }

  return {
    i18n,
  }
}
