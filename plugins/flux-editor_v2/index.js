import Vue from 'vue'
import FluxEditor from './FluxEditor'
import FluxComponentPlus from './FluxComponentPlus'
import FluxComponentMenu from './FluxComponentMenu'
import FluxPluginImage from './FluxPluginImage'
import FluxPluginVideo from './FluxPluginVideo'
import FluxPluginCode from './FluxPluginCode'
import FluxPluginAudio from './FluxPluginAudio'
import FluxPluginSection from './FluxPluginSection'

Vue.component('FluxEditorCore', FluxEditor)
Vue.component('FluxComponentPlus', FluxComponentPlus)
Vue.component('FluxComponentMenu', FluxComponentMenu)
Vue.component('FluxPluginImage', FluxPluginImage)
Vue.component('FluxPluginVideo', FluxPluginVideo)
Vue.component('FluxPluginAudio', FluxPluginAudio)
Vue.component('FluxPluginCode', FluxPluginCode)
Vue.component('FluxPluginSection', FluxPluginSection)

Vue.prototype.$fluxEditorEventBus = new Vue()
