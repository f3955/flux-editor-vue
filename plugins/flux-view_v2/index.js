import Vue from 'vue'
import FluxView from './FluxView'
import FluxViewRender from './FluxViewRender'
import FluxViewImage from './FluxViewImage'
import FluxViewVideo from './FluxViewVideo'
import FluxViewAudio from './FluxViewAudio'
import FluxViewCode from './FluxViewCode'

Vue.component('FluxView', FluxView)
Vue.component('FluxViewRender', FluxViewRender)
Vue.component('FluxViewImage', FluxViewImage)
Vue.component('FluxViewVideo', FluxViewVideo)
Vue.component('FluxViewAudio', FluxViewAudio)
Vue.component('FluxViewCode', FluxViewCode)
